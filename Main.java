import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        System.out.printf("Number of possible groups: %d", findNoOfGroups(input));
    }

    private static int findNoOfGroups(String input) {
        Integer p = 0;
        Integer c = 0;
        Integer m = 0;
        Integer b = 0;
        Integer z = 0;

        for (char x : input.toCharArray()) {
            switch (x) {
                case 'p' : p++; break;
                case 'c' : c++; break;
                case 'm' : m++; break;
                case 'b' : b++; break;
                case 'z' : z++; break;
            }
        }

        return Collections.min(Arrays.asList(p, c, m, b, z));
    }
}
